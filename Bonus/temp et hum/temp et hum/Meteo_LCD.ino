/*
 Name:		temp_et_hum.ino
 Created:	21/09/2019 02:50:12
 Author:	Louc
 http://www.bien-programmer.fr/pthreads.htm
 https://forum.hardware.fr/hfr/Programmation/C/executer-plusieurs-boucles-sujet_121457_1.htm
*/

// the setup function runs once when you press reset or power the board

#include <dht.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
//#include <arduinothred>
#include <Thread.h>



//#include <dht.h>
//#include <LiquidCrystalh>
//include <Wire.h>
//#include <LiquidCrystal_I2C.h>

//LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

const int buzzer = 9; //buzzer to arduino pin 9

LiquidCrystal_I2C lcd(0x27, 20, 4);
dht DHT;

#define DHT11_PIN 7

static void * buzzSequence()
{
	tone(buzzer, 1000); // Send 1KHz sound signal...
	delay(1000);        // ...for 0.3 sec
	noTone(buzzer);     // Stop sound...
	delay(500);
	return NULL;

}

static void * stroboLed()
{
	delay(100);
	digitalWrite(2, LOW);
	digitalWrite(4, LOW);
	delay(200);
	digitalWrite(2, HIGH);
	digitalWrite(4, HIGH);
	delay(200);
	digitalWrite(2, LOW);
	digitalWrite(4, LOW);
	delay(200);
	digitalWrite(2, HIGH);
	digitalWrite(4, HIGH);
	delay(200);
	digitalWrite(2, LOW);
	digitalWrite(4, LOW);
	delay(200);
	digitalWrite(2, HIGH);
	digitalWrite(4, HIGH);
	delay(200);
	digitalWrite(2, LOW);
	digitalWrite(4, LOW);
	delay(200);
	digitalWrite(2, HIGH);
	digitalWrite(4, HIGH);
	delay(200);
	digitalWrite(2, LOW);
	digitalWrite(4, LOW);
	delay(200);
	return NULL;
}
void setup()
{

	lcd.init();
	// Print a message to the LCD.
	lcd.backlight();
	//lcd.begin(16, 2);
	pinMode(buzzer, OUTPUT); // Set buzzer - pin 9 as an output
	pinMode(2, OUTPUT); // Setup the LED
	pinMode(4, OUTPUT); // Setup the LED
}

void loop()
{
	int chk = DHT.read11(DHT11_PIN);
	digitalWrite(2, LOW);
	digitalWrite(4, LOW);
	lcd.setCursor(1, 0);
	lcd.print("Temp: ");
	lcd.print(DHT.temperature);
	lcd.print((char)223);
	lcd.print("C");
	lcd.setCursor(0, 1);
	lcd.print("Humidity: ");
	lcd.print(DHT.humidity);
	lcd.print("%");
	delay(2000);
	lcd.setCursor(1, 0);
	lcd.print("Voici la meteo");
	
	stroboLed() ,buzzSequence();
	
}