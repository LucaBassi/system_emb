#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

void *thread_1(void *arg)
{
    printf("Nous sommes dans le thread.\n");

    /* Pour enlever le warning */
    (void) arg;
    pthread_exit(NULL);
}

int main(void)
{
    int bouton=0;

    pthread_t thread1;

    printf("Avant la cr�ation du thread.\n");

    if (pthread_create(&thread1, NULL, thread_1, NULL)) {
	perror("pthread_create");
	return EXIT_FAILURE;
    }

    if (pthread_join(thread1, NULL)) {
	perror("pthread_join");
	return EXIT_FAILURE;
    }

    printf("Apr�s la cr�ation du thread.\n");

    return EXIT_SUCCESS;
}
