#include <Bounce2.h>


int trim = 0;
int val = 0;

int redPin1 = 9;
int greenPin1 = 10;
int bluePin1 = 11;

int redPin2 = 3;
int greenPin2 = 5;
int bluePin2 = 6;

int red1 = 0;
int red2 = 0;
int redResult = 0;

int green1 = 0;
int green2 = 0;
int greenResult = 0;

int blue1 = 0;
int blue2 = 0;
int blueResult = 0;

int button_1State = LOW; //position of button No1
int button_2State = LOW; //position of button No4

int switchSelector = 0; //switch selector

Bounce debouncer_1 = Bounce(); // Instantiate a Bounce object
Bounce debouncer_2 = Bounce(); // Instantiate a Bounce object

void setup()
{
	Serial.begin(9600);

	pinMode(redPin1, OUTPUT);
	pinMode(greenPin1, OUTPUT);
	pinMode(bluePin1, OUTPUT);

	pinMode(redPin2, OUTPUT);
	pinMode(greenPin2, OUTPUT);
	pinMode(bluePin2, OUTPUT);

	debouncer_1.attach(2, INPUT_PULLUP); // Attach the debouncer to a pin with INPUT_PULLUP mode
	debouncer_1.interval(25); // Use a debounce interval of 25 milliseconds

	debouncer_2.attach(8, INPUT_PULLUP); // Attach the debouncer to a pin with INPUT_PULLUP mode
	debouncer_2.interval(25); // Use a debounce interval of 25 milliseconds
}

void loop()
{
	val = analogRead(trim);

	//debouncer_1.update();          // Update the Bounce instance
	//debouncer_2.update();          // Update the Bounce instance


	button_1State = digitalRead(2);		//check state of button 1
	button_2State = digitalRead(8);	//check state of button 2


	if (debouncer_1.fell()) {switchSelector = 0;}
	if (debouncer_2.fell()) {switchSelector = 1;}

	

	else if(button_1State == LOW && button_2State == LOW)
	{
		redResult = (red1 + red2)/2;
		greenResult = (green1 + green2)/2;
		blueResult = (blue1 + blue2)/2;

		setColor1(redResult, greenResult, blueResult);
		setColor2(redResult, greenResult, blueResult);
		switchSelector = DEFAULT;
		delay(50);
	}
	debouncer_1.update();          // Update the Bounce instance
	debouncer_2.update();          // Update the Bounce instance


	switch (switchSelector) 
	{
		case 0:
			Serial.println(val);
			
			if (val <= 341)
			{
				setColor1(255, 0, 0);
				red1 = 255;
			}
			if (val > 341)
			{
				setColor1(0, 255, 0);
				green1 = 255;
			}
			if (val > 682)
			{
				setColor1(0, 0, 255);
				blue1 = 255;
			}
			break;
			
		case 1:
				Serial.println(val);
			if (val <= 341)
			{
				setColor2(255, 0, 0);
				red2 = 255;
			}
			if (val > 341)
			{
				setColor2(0, 255, 0);
				green2 = 255;
			}
			if (val > 682)
			{
				setColor2(0, 0, 255);
				blue2 = 255;
			}
		break;
		/*
		case 2:
				Serial.println(val);
				setColor1(redResult, greenResult, blueResult);
				setColor2(redResult, greenResult, blueResult);
			
		break;
		
		default: setColor1(0,0,0);
				 setColor2(0,0,0);
				 */
	}





}
void setColor1(int red, int green, int blue)
{
	analogWrite(redPin1, red);
	analogWrite(greenPin1, green);
	analogWrite(bluePin1, blue);
}

void setColor2(int red, int green, int blue)
{
	analogWrite(redPin2, red);
	analogWrite(greenPin2, green);
	analogWrite(bluePin2, blue);
}