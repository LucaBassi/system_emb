#include <Bounce2.h>

/****************************************************************************************************
*	Project: BinariCalc + ConsoleMessages															*
*	Author : Bassi Luca																				*
*	Date   : 17.09.19																				*
*	Version: 1.1																					*	
*	Description: This code is an application to calculate and display additions of little numbers	*
*	Requires :	- Arduino Uno																		*
*				- Eletronic shemas "05_Inputs_Outputs_Digital_base.fzz"								*
*	Include the Bounce2 library found here :														*
*	https://github.com/thomasfredericks/Bounce2														*
****************************************************************************************************/



void displayVal_1(int value) {       //this function translate number in birany and and enable correspondante Leds 

	for (int i = 2; i <= 5; i++)digitalWrite(i, LOW);

	if (value >= 8)digitalWrite(2, HIGH);
	value = value % 8;
	if (value >= 4)digitalWrite(3, HIGH);
	value = value % 4;
	if (value >= 2)digitalWrite(4, HIGH);
	value = value % 2;
	if (value >= 1)digitalWrite(5, HIGH);

}


void stroboLed() {   //This funcion do a slow strobo if number selected by user is bigger to 15 and smaller of 0
	for (int i = 0; i <= 3; i++) {
		delay(100);
		digitalWrite(2, LOW);
		digitalWrite(3, LOW);
		digitalWrite(4, LOW);
		digitalWrite(5, LOW);
		delay(200);
		digitalWrite(2, HIGH);
		digitalWrite(3, HIGH);
		digitalWrite(4, HIGH);
		digitalWrite(5, HIGH);
		delay(200);
	}
}


int button_1State = LOW; //position of button No1
int button_4State = LOW; //position of button No4

int ledState_1 = LOW;	//State of led No1
int ledState_2 = LOW;	//State of led No2
int ledState_3 = LOW;	//State of led No3
int ledState_4 = LOW;	//State of led No4

int counter = 0;		//temp stock for 1st number
//int counter_2 = 0;		//temp stock for second number

int chiffre1 = 0;		//display value to add at stock  

int stock = 0;			//stock value
int result = 0;			//result to display

int switchSelector = 0; //switch selector

Bounce debouncer_1 = Bounce(); // Instantiate a Bounce object
Bounce debouncer_2 = Bounce(); // Instantiate a Bounce object
Bounce debouncer_3 = Bounce(); // Instantiate a Bounce object
Bounce debouncer_4 = Bounce(); // Instantiate a Bounce object

void reset() {

	stock = 0;
	result = 0;
	counter = 0;
	chiffre1 = 0;
	debouncer_1.update();
	debouncer_4.update();
}



void setup() {

	debouncer_1.attach(8, INPUT_PULLUP); // Attach the debouncer to a pin with INPUT_PULLUP mode
	debouncer_1.interval(25); // Use a debounce interval of 25 milliseconds

	debouncer_2.attach(9, INPUT_PULLUP); // Attach the debouncer to a pin with INPUT_PULLUP mode
	debouncer_2.interval(25); // Use a debounce interval of 25 milliseconds

	debouncer_3.attach(10, INPUT_PULLUP); // Attach the debouncer to a pin with INPUT_PULLUP mode
	debouncer_3.interval(25); // Use a debounce interval of 25 milliseconds

	debouncer_4.attach(11, INPUT_PULLUP); // Attach the debouncer to a pin with INPUT_PULLUP mode
	debouncer_4.interval(25); // Use a debounce interval of 25 milliseconds

	pinMode(2, OUTPUT); // Setup the LED
	pinMode(3, OUTPUT); // Setup the LED
	pinMode(4, OUTPUT); // Setup the LED
	pinMode(5, OUTPUT); // Setup the LED

	digitalWrite(2, ledState_1); // Apply LED state
	digitalWrite(3, ledState_1); // Apply LED state
	digitalWrite(4, ledState_1); // Apply LED state
	digitalWrite(5, ledState_1); // Apply LED state

	Serial.begin(9600);         // Set communication rate to 9600 bauds     
}

void loop() {

	debouncer_1.update();          // Update the Bounce instance
	debouncer_2.update();          // Update the Bounce instance
	debouncer_3.update();          // Update the Bounce instance
	debouncer_4.update();          // Update the Bounce instance


	button_1State = digitalRead(8);		//check state of button 1
	button_4State = digitalRead(11);	//check state of button 4



	if (debouncer_1.rose()) {		// Call code if button transitions from HIGH to LOW
		switchSelector = 1;          //Select whats number to display depending witch pushed button  
		counter++;
		chiffre1 = counter;

		
		Serial.print("Vous allez ajouter ");		//show at user what number he want to add by a message in console
		Serial.print(counter, DEC);
		Serial.println(" a votre totale");

	}

	if (debouncer_2.rose()) {      // Call code if button transitions from LOW to HIGH  
		switchSelector = 1;          //Select whats number to display depending witch pushed button

		counter--;
		chiffre1 = counter;

		Serial.print("Vous allez ajouter ");	//show at user what number he want to add by a message in console
		Serial.print(counter, DEC);
		Serial.println(" a votre totale");
	}


	if (debouncer_3.rose()) {			// Call code if button transitions from HIGH to LOW
		Serial.print("la valeur ");		//show at user what number he want to add by a message in console
		Serial.print(chiffre1, DEC);
		Serial.println(" a ete ajoute a votre stock");

		stock = stock + chiffre1;
		counter = 0;
		chiffre1 = 0;
		switchSelector = 0;	//switch selector 
	}


	if (debouncer_4.rose()) {      // Call code if button transitions from HIGH to LOW
		result = stock;
		stock =0;
		switchSelector = 2;
		Serial.print("le resultat est ");
		Serial.println(result, DEC);
	}

	if (button_1State == LOW && button_4State == LOW) {		//reset all valu if user push buttons 1 and 4 at same time 
//	result = 0;
//	stock = 0;
//	chiffre1 = 0;
		stroboLed();
		reset();
		//delay(500);
		switchSelector = 0;				//switch selector
		debouncer_1.update();			// Update the Bounce instance
		debouncer_4.update();			// Update the Bounce instance

		Serial.println("Reset");		//show at user the state of system by a message in console

	}
	}

	if (counter > 15) {	//if user try to enter a number bigger than 15 he's warned that he can't by a message in console
		stroboLed();
		chiffre1 = 0;
		counter = 0;
		Serial.println("Mmmmh... ajouter plus que l affichage le permet ? original..");
	}

	if (counter < 0) {	//if user try to enter a negative he's warned that he can't by a message in console
		stroboLed();
		chiffre1 = 0;
		counter = 0;
		Serial.println("Une valeur negative ne peut pas etre ajoutee.. oui les dev sont des flemard !");
	}

	if (result >= 16) {		//if the result is bigger than 15 all values are reset and user is adevrted by a message in console
		stroboLed();
		result = 0;
		stock = 0;
		counter = 0;
		chiffre1 = 0;
		Serial.println("Oups..le resultat deborde ;) ");
	}


	switch (switchSelector)
	{
	case 0:
		displayVal_1(0);  // Example with number 9 converted into binary and displayed 
		break;

	case 1:
		displayVal_1(chiffre1);  // convert number entred by user and display it directly in binary  
		break;

	case 2:
		displayVal_1(result);  // convert the result and display it directly in binary
		break;
	}
}
