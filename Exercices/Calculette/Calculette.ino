
// Project: Calculette_binaire
// Author : Bassi Luca
// Date   : 13.09.19
// Version: 1.0
// Description: Binairi calc. without bounce.h library
  

/*
 * This function is designed to convert a number between 0 and 15 into binary 
 * and displayed with four LED connected to ports 2 to 5 
 * 
 * value : The input number between 0 to 15
 * No return (void) 
 */



void displayVal_1(int value){

    for(int i=2;i<=5;i++)digitalWrite(i,LOW);
    
    if (value >= 8)digitalWrite(5,HIGH);
    value = value%8;
    if (value >= 4)digitalWrite(4,HIGH);
    value = value%4;
    if (value >= 2)digitalWrite(3,HIGH);
    value = value%2;
    if (value >= 1)digitalWrite(2,HIGH);

}
/*
void displayVal_2(int value){

    for(int i=2;i<=5;i++)digitalWrite(i,LOW);
    
    if (value >= 8)digitalWrite(5,HIGH);
    value = value%8;
    if (value >= 4)digitalWrite(4,HIGH);
    value = value%4;
    if (value >= 2)digitalWrite(3,HIGH);
    value = value%2;
    if (value >= 1)digitalWrite(2,HIGH);

}

void displayVal_3(int value){

    for(int i=2;i<=5;i++)digitalWrite(i,LOW);
    
    if (value >= 8)digitalWrite(5,HIGH);
    value = value%8;
    if (value >= 4)digitalWrite(4,HIGH);
    value = value%4;
    if (value >= 2)digitalWrite(3,HIGH);
    value = value%2;
    if (value >= 1)digitalWrite(2,HIGH);

}

*/
void setup() {
  
 

  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);

  pinMode(8, INPUT_PULLUP);            // sets the digital pin 8 as input  
  pinMode(9, INPUT_PULLUP);     // sets the digital pin 9 as input 
  pinMode(10, INPUT_PULLUP);    // sets the digital pin 8 as input  
  pinMode(11, INPUT);    // sets the digital pin 9 as input 
  
  for(int i=2;i<=5;i++)pinMode(i,OUTPUT);
   
}

int chiffre1 =0;
int chiffre2=0;
int chiffreAffiche=0;
//int chiffreStock=0;
//int chiffre2Affiche=0;


int resultat=0;
  
int button_1State = 0; //postiton of buton no 1
int button_2State = 0; //postiton of buton no 2
int button_3State = 0; //postiton of buton no 3
int button_4State = 0; //postiton of buton no 4

void loop() {
  


 // int valButton1 = 0;
 // int valButton2 = 0;
 // int stockVal = 0;

  button_1State = digitalRead(8);   //ckeck the state of buton and put it in variable
  button_2State = digitalRead(9);   //ckeck the state of buton and put it in variable
  button_3State = digitalRead(10);   //ckeck the state of buton and put it in variable
  button_4State = digitalRead(11);   //ckeck the state of buton and put it in variable

  
  if(button_1State==LOW){
    	chiffre1 = chiffre1 + 1;
    	delay(50);
    	chiffreAffiche=1;
  }
  
  
  if(button_2State==LOW){
    	chiffre1 = chiffre1 - 1;
    	delay(50);
    	chiffreAffiche=1;
  }
  
  
  if(button_3State==LOW){
  		chiffreAffiche=0;
    	resultat = resultat + chiffre1; 
    	delay(50);
    	chiffre1=0;
  }
  
  
  if(button_4State==LOW){
  		chiffreAffiche=2;
  }

  
  switch(chiffreAffiche)
  {
    case 0:
    	displayVal_1(0);  // Example with number 9 converted into binary and displayed 
    	break;
    
    case 1:
      	displayVal_1(chiffre1);  // Example with number 9 converted into binary and displayed 
    	break;
    
    case 2:
      	displayVal_1(resultat);  // Example with number 9 converted into binary and displayed 
    	break;
  }    
  
}
