#include <Bounce2.h>

// Tableau de notes et tableau des dur?es pour la m?lodie de la chanson "Au clair de la Lune" 
const int notesMario[] = { 2637, 2637, 0, 2637, 0, 2093, 2637, 0, 3136, 0, 0, 0, 1568, 0, 0, 0 };
const int durationMario = 120;





boolean pushedButon[4] = { 0,0,0,0 };

boolean alightLed[31][4] = {0,0,0,0,
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0, 
							0,0,0,0};



int button_1State =HIGH; //position of button No1
int button_2State =HIGH; //position of button No1
int button_3State =HIGH; //position of button No1
int button_4State =HIGH; //position of button No1

Bounce debouncer_1 = Bounce(); // Instantiate a Bounce object
Bounce debouncer_2 = Bounce(); // Instantiate a Bounce object
Bounce debouncer_3 = Bounce(); // Instantiate a Bounce object
Bounce debouncer_4 = Bounce(); // Instantiate a Bounce object


const int kPinLED_1 = 2;  // LED connected to to Digital Pin 1
const int kPinLED_2 = 3;  // LED connected to to Digital Pin 1
const int kPinLED_3 = 4;  // LED connected to to Digital Pin 1
const int kPinLED_4 = 5;  // LED connected to to Digital Pin 1

int valToDisplay = 1;


void setup()
{
	long randNumber;

	 
		Serial.begin(9600);
		randomSeed(analogRead(A2));
	

	debouncer_1.attach(A2); // Attach the debouncer to a pin with INPUT_PULLUP mode
	debouncer_1.interval(25); // Use a debounce interval of 25 milliseconds

	debouncer_2.attach(A3); // Attach the debouncer to a pin with INPUT_PULLUP mode
	debouncer_2.interval(25); // Use a debounce interval of 25 milliseconds

	debouncer_3.attach(A4); // Attach the debouncer to a pin with INPUT_PULLUP mode
	debouncer_3.interval(25); // Use a debounce interval of 25 milliseconds

	debouncer_4.attach(A5); // Attach the debouncer to a pin with INPUT_PULLUP mode
	debouncer_4.interval(25); // Use a debounce interval of 25 milliseconds


	pinMode(A2, INPUT); // Generally, in push-button we take INPUT as a parameter but here we take OUTPUT because ANALOG PIN 
	pinMode(A3, INPUT); // Generally, in push-button we take INPUT as a parameter but here we take OUTPUT because ANALOG PIN 
	pinMode(A4, INPUT); // Generally, in push-button we take INPUT as a parameter but here we take OUTPUT because ANALOG PIN 
	pinMode(A5, INPUT); // Generally, in push-button we take INPUT as a parameter but here we take OUTPUT because ANALOG PIN 


	digitalWrite(A2, HIGH); // Make button condition HIGH
	digitalWrite(A3, HIGH); // Make button condition HIGH
	digitalWrite(A4, HIGH); // Make button condition HIGH
	digitalWrite(A5, HIGH); // Make button condition HIGH


	pinMode(kPinLED_1, OUTPUT);
	pinMode(kPinLED_2, OUTPUT);
	pinMode(kPinLED_3, OUTPUT);
	pinMode(kPinLED_4, OUTPUT);
}

void loop()
{
	int randNumber = random(4);

	int randPut0_1 = random(1);
	/*
	for (int i = 0; i < 30; i++) {
		alightLed[i] = 1;
	}
	*/

	for (int i = 0; i < 31; i++) {
		alightLed[i][randNumber] == 1;
	};


	for (int i = 0; i < 30; i++) {
		
		for (int j = 0; j > 3; j++) {
			alightLed[i][j] == randPut0_1;
			Serial.println(alightLed[i][j]);
		};
	};


	debouncer_1.update();

	button_1State = analogRead(A2);   //check state of button 1
	button_2State = analogRead(A3);   //check state of button 2
	button_3State = analogRead(A4);   //check state of button 3
	button_4State = analogRead(A5);   //check state of button 4



	// Now, normal push-button operation, LED blink when we press push button
	/*
	if (debouncer_1.fell()) {

		valToDisplay++;
		Serial.println(valToDisplay);
		for (int i = 0; i < 16; i++) {

			//int duree = dureeClairLune[i];
		//	tone(6, notesMario[i]);
			delay(durationMario);
			noTone(6);
			delay(i + (i * 1.3));

		}
		noTone(6);
		debouncer_1.update();          // Update the Bounce instance
	
	}
	*/


	
//	alightLed[0] = 1;


	if (debouncer_1.fell()) {
		pushedButon[0] = 1;
		alightLed[0][0] = 1;
		
		Serial.println(alightLed[0][0]);
		}
		debouncer_1.update();          // Update the Bounce instance

		if( (pushedButon[0] && alightLed[0])==1) {

			digitalWrite(kPinLED_1, HIGH);
			delay(50);
			digitalWrite(kPinLED_1, LOW);
			delay(50);
			digitalWrite(kPinLED_1, HIGH);
			delay(50);
			digitalWrite(kPinLED_1, LOW);
			delay(50);
			digitalWrite(kPinLED_1, HIGH);
			delay(50);
			digitalWrite(kPinLED_1, LOW);
		}
	

	if (debouncer_2.fell()) {
		pushedButon[1] = 1;
		alightLed[0][1] = 1;
		
		Serial.println(alightLed[0][1]);
		}
		debouncer_2.update();          // Update the Bounce instance

		if( (pushedButon[1] && alightLed[1])==1) {

			digitalWrite(kPinLED_2, HIGH);
			delay(50);
			digitalWrite(kPinLED_2, LOW);
			delay(50);
			digitalWrite(kPinLED_2, HIGH);
			delay(50);
			digitalWrite(kPinLED_2, LOW);
			delay(50);
			digitalWrite(kPinLED_2, HIGH);
			delay(50);
			digitalWrite(kPinLED_2, LOW);
		}
	

	if (debouncer_1.fell()) {
		pushedButon[0] = 1;
		
		Serial.println(alightLed[0][0]);
		}
		debouncer_1.update();          // Update the Bounce instance

		if( (pushedButon[0] && alightLed[0])==1) {

			digitalWrite(kPinLED_1, HIGH);
			delay(50);
			digitalWrite(kPinLED_1, LOW);
			delay(50);
			digitalWrite(kPinLED_1, HIGH);
			delay(50);
			digitalWrite(kPinLED_1, LOW);
			delay(50);
			digitalWrite(kPinLED_1, HIGH);
			delay(50);
			digitalWrite(kPinLED_1, LOW);
		}
	

	if (debouncer_1.fell()) {
		pushedButon[0] = 1;
		
		Serial.println(alightLed[0][0]);
		}
		debouncer_1.update();          // Update the Bounce instance

		if( (pushedButon[0] && alightLed[0])==1) {

			digitalWrite(kPinLED_1, HIGH);
			delay(50);
			digitalWrite(kPinLED_1, LOW);
			delay(50);
			digitalWrite(kPinLED_1, HIGH);
			delay(50);
			digitalWrite(kPinLED_1, LOW);
			delay(50);
			digitalWrite(kPinLED_1, HIGH);
			delay(50);
			digitalWrite(kPinLED_1, LOW);
		}
	

		pushedButon[0] = 0;
		pushedButon[1] = 0;
		pushedButon[2] = 0;
		pushedButon[3] = 0;
}
	






