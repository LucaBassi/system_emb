/*
 Name:		Sketch1.ino
 Created:	07/10/2019 10:15:42
 Author:	Luca.BASSI
*/

#include <Bounce2.h>
/*
int redPin1 = 9;
int greenPin1 = 10;
int bluePin1 = 11;
*/
const int notesMario[] = { 2637, 2637, 0, 2637, 0, 2093, 2637, 0, 3136, 0, 0, 0, 1568, 0, 0, 0 };
const int durationMario = 120;

// Tableau de notes et tableau des dur�es pour la m�lodie de la chanson "Au clair de la Lune" 
const int notesClairLune[] = { 262, 262, 262, 294, 330, 294, 262, 330, 294, 294, 262 };
const int dureeClairLune[] = { 400, 400, 400, 400, 800, 800, 400, 400, 400, 400, 1200 };


int button_1State = LOW; //position of button No1
int button_2State = LOW; //position of button No4
int button_3State = LOW; //position of button No1
int button_4State = LOW; //position of button No4

int switchSelector = 0; //switch selector

Bounce debouncer_1 = Bounce(); // Instantiate a Bounce object
Bounce debouncer_2 = Bounce(); // Instantiate a Bounce object
Bounce debouncer_3 = Bounce(); // Instantiate a Bounce object
Bounce debouncer_4 = Bounce(); // Instantiate a Bounce object

// the setup function runs once when you press reset or power the board
void setup() {


	pinMode(6, OUTPUT);

	debouncer_1.attach(2, INPUT_PULLUP); // Attach the debouncer to a pin with INPUT_PULLUP mode
	debouncer_1.interval(25); // Use a debounce interval of 25 milliseconds

	debouncer_2.attach(3, INPUT_PULLUP); // Attach the debouncer to a pin with INPUT_PULLUP mode
	debouncer_2.interval(25); // Use a debounce interval of 25 milliseconds

	debouncer_3.attach(4, INPUT_PULLUP); // Attach the debouncer to a pin with INPUT_PULLUP mode
	debouncer_3.interval(25); // Use a debounce interval of 25 milliseconds

	debouncer_4.attach(5, INPUT_PULLUP); // Attach the debouncer to a pin with INPUT_PULLUP mode
	debouncer_4.interval(25); // Use a debounce interval of 25 milliseconds
}

// the loop function runs over and over again until power down or reset
void loop() {

	button_1State = digitalRead(2);   //check state of button 1
	button_2State = digitalRead(3);   //check state of button 2
	button_3State = digitalRead(4);   //check state of button 1
	button_4State = digitalRead(5);   //check state of button 2

	if (debouncer_1.fell()) {
		for (int i = 0; i < 11; i++) {
			
			//int duree = dureeClairLune[i];
			Serial.println(notesClairLune[i]);
			Serial.println(dureeClairLune[i]);
			tone(6, notesClairLune[i]);
			delay(dureeClairLune[i]);
			//noTone(6);
			//delay(i + (i * 1.3));

		}
		debouncer_1.update();          // Update the Bounce instance
		noTone(6);
	}


	if (debouncer_2.fell()) {
		for (int i = 0; i < 16; i++) {

			//int duree = dureeClairLune[i];
			Serial.println(notesMario[i]);
			Serial.println(durationMario);
			tone(6, notesMario[i]);
			delay(durationMario);
			//noTone(6);
			//delay(i + (i * 1.3));

		}
		noTone(6);
		debouncer_1.update();          // Update the Bounce instance
		
	}


	debouncer_1.update();          // Update the Bounce instance
	debouncer_2.update();          // Update the Bounce instance
	debouncer_3.update();          // Update the Bounce instance
	debouncer_4.update();          // Update the Bounce instance

	


}